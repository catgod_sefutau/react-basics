import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import List from './components/List.jsx';
import Modal from './components/Modal.jsx';
import API from './helpers/fakeAPI';

class App extends Component {
  constructor(props) {
    super(props);
    this.lists = API.getTaskLists();
    this.state = {
      lists : this.lists
    }
  }

  addTask = (listTitle, taskName, taskText) => {
    API.addTask(listTitle, taskName, taskText).then(() => {
      this.setState({
        list: API.getTaskLists()
      })
    })
  }

  addList = (listTitle) => {
    API.addList(listTitle).then(() => {
      this.setState({
        list: API.getTaskLists()
      })
    })
  }

  render() {
    const lists = this.state ? this.state.lists : [];
    let listKey = 0;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to 🅱eact</h1>
        </header>
        {lists.map( (currentTaskList) => 
          <List taskList={currentTaskList} key={listKey++}/> 
        )}
        <Modal taskLists={lists} addTask={this.addTask} addList={this.addList}></Modal>
      </div>
    );
  }
}

export default App;
 