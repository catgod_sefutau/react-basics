import React, { Component } from 'react';
import Task from './Task.jsx';

class List extends Component {
  constructor(props) {
    super(props);

    this.title = this.props.taskList.title;
    this.taskArray = this.props.taskList.taskArray;
  }

  render() {
    const {title, taskArray} = this;
    let taskIndex = -1;
    return (
      <div className="task-component">
        <h2 className="task-component-title">{title}:</h2>
        <ul className="task-component-list">
          {taskArray.map( (currentTask) => 
          <li key={++taskIndex}>
            <Task task={currentTask} crtIndex={taskIndex} toggleCheck={this.toggleCheck}/>
          </li>
          )}
        </ul>
      </div>
    );
  }
}

export default List;
 