import React, { Component } from 'react';

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type : 'CLOSED'
    };

    this.nameValue = '';
  }

  initForm() {
    this.listValue = '';
    this.setState({
      type : 'INITIAL',
      nameValue : ''
    });
  }

  changeName = (newValue) => {
    this.nameValue = newValue;
  };

  changeText = (newValue) => {
    this.textValue = newValue;
  };

  changeList = (newValue) => {
    this.listValue = newValue;
  };

  changeType = (newValue) => {
    this.setState({
      type: newValue
    })
  };

  submitForm = () => {
    const {type} = this.state;
    const {addTask, addList} = this.props;

    if (type == 'TASK') {
      addTask(this.listValue, this.nameValue, this.textValue);
      this.forceUpdate();
    } else if (type == 'LIST') {
      addList(this.nameValue);
      this.forceUpdate();
    }
  }

  render() {
    const {taskName, type} = this.state;
    
    switch (type) {  
      case 'INITIAL':

        return (
        <div>
         <div className="modal-component-open" onClick={() => this.changeType('INITIAL')}>+</div>
          <div className="modal-component">
            <button className="modal-component-close" onClick={() => this.changeType('CLOSED')}>X</button>
            <h2 className="modal-component-title">New List or Task:</h2>
            <div className="modal-component-form">
              <div className="modal-component-form-field">
                <label>Type of new entity:</label>
                <select defaultValue='INITIAL' onChange={(currentDOMelement) => this.changeType(currentDOMelement.target.value)}>
                  <option value='INITIAL'>Pick an option</option>
                  <option value='LIST'>List</option>
                  <option value='TASK'>Task</option>
                </select>
              </div>
            </div>
            <div className="modal-component-buttons">
              <button className="button cancel" onClick={() => this.changeType('CLOSED')}>Cancel</button>
            </div>
          </div>
        </div>);
        break;

      case 'LIST':
        return (
        <div>
         <div className="modal-component-open" onClick={() => this.changeType('INITIAL')}>+</div>
          <div className="modal-component">
            <button className="modal-component-close" onClick={() => this.changeType('CLOSED')}>X</button>
            <h2 className="modal-component-title">New List:</h2>
            
            <div className="modal-component-form">  
              <div className="modal-component-form-field">
                <div className="modal-component-form">
                  <label>List Name</label>
                  <input onChange={(currentDOMelement) => this.changeName(currentDOMelement.target.value)}/>
                </div>
              </div>
            </div>
            <div className="modal-component-buttons">
              <button className="button confirm" onClick={ this.submitForm }>Ok</button>
              <button className="button cancel" onClick={() => this.changeType('INITIAL')}>Cancel</button>
            </div>
          </div>
        </div>);
        
      case 'TASK':
        return (
        <div>
         <div className="modal-component-open" onClick={() => this.changeType('INITIAL')}>+</div>
          <div className="modal-component">
            <button className="modal-component-close" onClick={() => this.changeType('CLOSED')}>X</button>
            <h2 className="modal-component-title">New Task:</h2>
            
            <div className="modal-component-form">
              <div className="modal-component-form-field">
               <label>List:</label>
                <select defaultValue='INITIAL' onChange={(currentDOMelement) => this.changeList(currentDOMelement.target.value)}>
                  <option value='INITIAL' disabled>Pick an option</option>
                  {this.props.taskLists.map( (list) => (
                    <option value={list.title}> {list.title} </option>
                  ))}
                </select>
              </div>
              <div className="modal-component-form-field">
               <label>Task Name</label>
                <div className="modal-component-form">
                  <input onChange={(currentDOMelement) => this.changeName(currentDOMelement.target.value)}/>
                </div>
              </div>
              <div className="modal-component-form-field">
               <label>Task Text</label>
                <div className="modal-component-form">
                  <input onChange={(currentDOMelement) => this.changeText(currentDOMelement.target.value)}/>
                </div>
              </div>
            </div> 
            <div className="modal-component-buttons">
              <button className="button confirm" onClick={ this.submitForm }>Ok</button>
              <button className="button cancel" onClick={() => this.changeType('INITIAL')}>Cancel</button>
            </div>
          </div>
        </div>);

      default:
        return (<div className="modal-component-open" onClick={() => this.changeType('INITIAL')}>+</div>);
    }
  }
}

export default Modal;
 