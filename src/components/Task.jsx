import React, { Component } from 'react';

class Task extends Component {
  constructor(props) {
    super(props);

    this.text = this.props.task.text;
    this.title = this.props.task.title;
    this.checked = this.props.task.checked;
    this.state = {
      checked: this.checked
    };
  }

  toggleToDo = () => {
    this.setState({
      checked: !this.state.checked
    })
  }

  render() {
    const {title, text} = this.props.task;
    const {checked} = this.state?this.state:{};

    if (!this.state) 
      return (
        <div>Loading...</div>
      );

    return (
      <div className="task">
        <h5 className="task-title">{title}:</h5>
        <p className="task-text">{text}</p>
        <div className={`task-checkbox checked-${checked}`} onClick={this.toggleToDo}>
          
        </div>
      </div>
    );
  }
}

export default Task;
 